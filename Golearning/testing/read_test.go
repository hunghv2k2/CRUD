package testing

import (
	"Gogood/model"
	golibdataTestUtil "gitlab.com/golibs-starter/golib-data/testutil"
	golibtest "gitlab.com/golibs-starter/golib-test"
	"net/http"
	"testing"
)

func TestReadById_Pass(t *testing.T) {
	get := []*model.Restaurants{
		{
			Id:      1,
			Name:    "resabc1",
			Address: "hanoi",
		},
		{
			Id:      3,
			Name:    "resabc3",
			Address: "haiphong",
		},
	}

	golibdataTestUtil.TruncateTablesOpt(model.Restaurants{}.TableName())
	golibdataTestUtil.Insert(get)
	golibtest.NewRestAssured(t).
		When().
		Get("/items/:id").
		Then().
		Status(http.StatusOK).
		Body("meta.code", 200).
		Body("model.Restaurants{}.Id", get)
}
