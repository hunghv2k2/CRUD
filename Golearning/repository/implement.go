package repository

import (
	"Gogood/model"
	"context"
)

func (s *sqlRepo) Create(ctx context.Context, data *model.RestaurantsCreate) error {
	if err := s.db.Create(&data).Error; err != nil {
		return err
	}

	return nil
}

func (s *sqlRepo) Update(ctx context.Context, data *model.RestaurantsUpdate, id int) error {
	if err := s.db.Table(model.RestaurantsUpdate{}.
		TableName()).
		Where("id = ?", id).
		Updates(&data).Error; err != nil {
		return err
	}

	return nil
}

func (s *sqlRepo) Delete(ctx context.Context, id int) error {
	if err := s.db.Table(model.Restaurants{}.TableName()).
		Where("id = ?", id).
		Delete(nil).Error; err != nil {
		return err
	}

	return nil
}

func (s *sqlRepo) Read(
	ctx context.Context,
	id int,

) (*model.Restaurants, error) {
	var data model.Restaurants

	if err := s.db.Where("id = ?", id).First(&data).Error; err != nil {
		return nil, err
	}

	return &data, nil
}
