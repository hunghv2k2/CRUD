package appctx

import "gorm.io/gorm"

type AppContext interface {
	GetDBConnect() *gorm.DB
}

type appCtx struct {
	db *gorm.DB
}

func NewAppContext(db *gorm.DB) *appCtx {
	return &appCtx{db: db}
}

func (cntxt appCtx) GetDBConnect() *gorm.DB {
	return cntxt.db
}
