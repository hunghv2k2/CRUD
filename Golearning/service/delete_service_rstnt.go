package service

import "context"

type DeleteRestaurantsRepo interface {
	Delete(ctx context.Context, id int) error
}

type deleteRestaurantsService struct {
	repo DeleteRestaurantsRepo
}

func NewDeleteRestaurantsService(repo DeleteRestaurantsRepo) *deleteRestaurantsService {
	return &deleteRestaurantsService{repo: repo}
}

func (src *deleteRestaurantsService) DeleteRestaurants(context context.Context, id int) error {
	if err := src.repo.Delete(context, id); err != nil {
		return err
	}

	return nil
}
