package service

import (
	"Gogood/model"
	"context"
	"errors"
)

type CreateRestaurantsRepo interface {
	Create(ctx context.Context, data *model.RestaurantsCreate) error
}

type createRestaurantsService struct {
	repo CreateRestaurantsRepo
}

func NewCreateRestaurantsService(repo CreateRestaurantsRepo) *createRestaurantsService {
	return &createRestaurantsService{repo: repo}
}

func (src *createRestaurantsService) CreateRestaurants(
	context context.Context,
	data *model.RestaurantsCreate,
) error {
	if data.Name == "" {
		errors.New("name can't be empty")
	}

	if err := src.repo.Create(context, data); err != nil {
		return err
	}

	return nil
}
