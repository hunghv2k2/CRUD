package service

import (
	"Gogood/model"
	"context"
)

type UpdateRestaurantsRepo interface {
	Update(ctx context.Context, data *model.RestaurantsUpdate, id int) error
}

type updateRestaurantsService struct {
	repo UpdateRestaurantsRepo
}

func NewUpdateRestaurantsService(repo UpdateRestaurantsRepo) *updateRestaurantsService {
	return &updateRestaurantsService{repo: repo}
}

func (src *updateRestaurantsService) UpdateRestaurants(
	context context.Context,
	data *model.RestaurantsUpdate,
	id int,
) error {
	if err := src.repo.Update(context, data, id); err != nil {
		return err
	}

	return nil
}
